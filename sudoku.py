
H = [1,1,1, 1,1,1, 1,1,1]
_ = [0,0,0, 0,0,0, 0,0,0]

PATTERNS = [
    [H,_,_, _,_,_, _,_,_,],
    [_,H,_, _,_,_, _,_,_,],
    [_,_,H, _,_,_, _,_,_,],
    [_,_,_, H,_,_, _,_,_,],
    [_,_,_, _,H,_, _,_,_,],
    [_,_,_, _,_,H, _,_,_,],
    [_,_,_, _,_,_, H,_,_,],
    [_,_,_, _,_,_, _,H,_,],
    [_,_,_, _,_,_, _,_,H,],

    [ [1,0,0,0,0,0,0,0,0] for x in range(9) ],
    [ [0,1,0,0,0,0,0,0,0] for x in range(9) ],
    [ [0,0,1,0,0,0,0,0,0] for x in range(9) ],
    [ [0,0,0,1,0,0,0,0,0] for x in range(9) ],
    [ [0,0,0,0,1,0,0,0,0] for x in range(9) ],
    [ [0,0,0,0,0,1,0,0,0] for x in range(9) ],
    [ [0,0,0,0,0,0,1,0,0] for x in range(9) ],
    [ [0,0,0,0,0,0,0,1,0] for x in range(9) ],
    [ [0,0,0,0,0,0,0,0,1] for x in range(9) ],

    [[1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
     _,_,_,_,_,_
    ],
    [[0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
     _,_,_,_,_,_
    ],
    [[0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
     _,_,_,_,_,_
    ],

    [_,_,_,
     [1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
     _,_,_
    ],
    [_,_,_,
     [0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
     _,_,_
    ],
    [_,_,_,
     [0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
     _,_,_
    ],

    [_,_,_,_,_,_,
     [1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
     [1,1,1, 0,0,0, 0,0,0,],
    ],
    [_,_,_,_,_,_,
     [0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
     [0,0,0, 1,1,1, 0,0,0,],
    ],
    [_,_,_,_,_,_,
     [0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
     [0,0,0, 0,0,0, 1,1,1,],
    ],
]

RELATIONS = []
for p in PATTERNS:
    relation = []
    k = 0
    for y, line in enumerate(p):
        for x, v in enumerate(line):
            if v:
                relation.append(k)
            k += 1
    RELATIONS.append(relation)


RELATION_BY_CELL_ID = []
for y in range(9):
    for x in range(9):
        RELATION_BY_CELL_ID.append([])

for relation in RELATIONS:
    for cell_id in relation:
        RELATION_BY_CELL_ID[cell_id].append(relation)



class Cell(object):

    def __init__(self, row, column, pk):
        self.row = row
        self.column = column
        self.pk = pk
        self.total_possibles = 9
        self.possibles = dict([ (x, True) for x in range(1, 10) ])
        self.value = None
        self.manual = False

    def list_possibles(self):
        line = []
        for k, v in self.possibles.items():
            if v:
                line.append(str(k))
        return ', '.join(line)

    @classmethod
    def get_id(cls, row, column):
        return row * 9 + column

    def list_choices(self, ):
        for i in range(1, 10):
            v = self.possibles.get(i)
            yield {
                'number': i,
                'possible': v,
            }


class Board(object):
    def __init__(self, text=None):
        self.cells = []
        for y in range(9):
            for x in range(9):
                self.cells.append(Cell(y, x, pk=Cell.get_id(y, x)))
        if text:
            self.load_game(text)


    def is_valid(self, ):
        for r_id, relation in enumerate(RELATIONS):
            found = {}
            for cell_id in relation:
                c = self.cells[cell_id]
                if c.value:
                    if found.get(c.value):
                        print('INVALID! ', c.value, 'rel:', relation)
                        return False
                    found[c.value] = True
        return True


    def _kill_value(self, cell, value):
        if cell.possibles.get(value):
            cell.possibles[value] = False
            cell.total_possibles -= 1
            if cell.total_possibles <= 1:
                for k, v in cell.possibles.items():
                    if v:
                       self._set_cell(cell, k)

    def _set_cell(self, cell, value):
        if cell.value is None:
            cell.value = value
            cell.possibles = {}
            cell.total_possibles = 0

            for relation in RELATION_BY_CELL_ID[cell.pk]:
                for cell_id in relation:
                    self._kill_value(self.cells[cell_id], value)


    def get_cell(self, location):
        return self.cells[Cell.get_id(*location)]

    def set_cell(self, location, value):
        self._set_cell(self.get_cell(location), value)

    def print(self):
        k = 0
        for y in range(9):
            line = []
            for x in range(9):
                line.append(str(self.cells[k].value or '.'))
                k += 1
            print(' '.join(line))

    def load_game(self, text):
        lines = text.strip().split('\n')
        k = 0
        for y, line in enumerate(lines):
            for x, value in enumerate(line):
                value = value.strip()
                if value != '.':
                    self._set_cell(self.cells[k], int(value))
                k += 1

    def run_single_choice(self, ):
        changed = False
        for relation in RELATIONS:
            counters = {}
            last_cells = {}
            for cell_id in relation:
                c = self.cells[cell_id]
                if c.value:
                    counters[c.value] = 10
                else:
                    for number, possible in c.possibles.items():
                        if possible:
                            counters[number] = counters.get(number, 0) + 1
                            last_cells[number] = c
            for number, counter in counters.items():
                if counter == 1:
                    self._set_cell(last_cells[number], number)
                    changed = True
        return changed



import os
from flask import Flask, request
from jinja2 import Environment, FileSystemLoader, select_autoescape


app = Flask(__name__)
env = Environment(
    loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    autoescape=select_autoescape(['html', 'xml'])
)

@app.route("/", methods=['GET', 'POST'])
def home():
    template = env.get_template('home.html')

    board = Board()
    rows = []
    k = 0
    for r in range(0, 9):
        for c in range(0, 9):
            intent = request.form.get('intent-%ld' % k, '')
            if intent != 'clear':
                v = request.form.get('cell-%ld' % k, '')
                if v:
                    board.set_cell((r,c), int(v))
                    board.get_cell((r,c)).manual = True
            k += 1

    while board.run_single_choice():
        pass

    k = 0
    for r in range(0, 9):
        cells = []
        for c in range(0, 9):
            cell = board.cells[k]
            cells.append({
                'pk': k,
                'column': c,
                'value': cell.value or '',
                'manual': cell.manual,
                'choices': list(cell.list_choices()),
            })
            k += 1
        rows.append({
            'pk': r,
            'cells': cells,
        })
    return template.render(rows=rows, board=board)
